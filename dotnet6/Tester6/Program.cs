﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Text.Json;
using System.Reflection;
using System.Net.Sockets;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using System.Globalization;
using System.Net.NetworkInformation;
using System.IO.Ports;

class Program
{
    static async Task Main(string[] args)
    {
        Console.WriteLine("Testing .NET features...");

        // Basic console output and input
        Console.WriteLine("Console output is working.");
        Console.Write("Enter something: ");
        string userInput = Console.ReadLine();
        Console.WriteLine($"You entered: {userInput}");

        // File I/O operations
        string testFilePath = "test.txt";
        File.WriteAllText(testFilePath, "This is a test file.");
        string fileContent = File.ReadAllText(testFilePath);
        Console.WriteLine($"File content: {fileContent}");

        // Multithreading
        await TestMultithreading();

        // HTTP requests
        await TestHttpRequest();

        // Asynchronous programming test
        await TestAsyncOperation();

        // Exception Handling Test
        TestExceptionHandling();

        // Dependency Injection Test
        TestDependencyInjection();

        // Localization and Globalization Test
        TestLocalization();

        // Memory Management Test
        TestMemoryManagement();

        // Custom Event Handlers Test
        TestEventHandling();

        // Test /dev/tty Accessibility
        TestDevTtyAccessibility();

        // USB Devices Test
        TestUsbDevices();

        // Test Network Devices Accessibility
        TestNetworkDevicesAccessibility();

        Console.WriteLine("All tests completed successfully.");
    }

    static async Task TestMultithreading()
    {
        Task[] tasks = new Task[3];
        for (int i = 0; i < 3; i++)
        {
            int taskId = i;
            tasks[i] = Task.Run(() =>
            {
                Console.WriteLine($"Task {taskId + 1} is running.");
                Thread.Sleep(1000); // Simulate work
                Console.WriteLine($"Task {taskId + 1} is completed.");
            });
        }
        await Task.WhenAll(tasks);
    }

    static async Task TestHttpRequest()
    {
        using (HttpClient client = new HttpClient())
        {
            HttpResponseMessage response = await client.GetAsync("https://jsonplaceholder.typicode.com/todos/1");
            string responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine($"HTTP response: {responseBody}");
        }
    }

    static async Task TestAsyncOperation()
    {
        await Task.Delay(1000);
        Console.WriteLine("Asynchronous operation test completed.");
    }

    static void TestExceptionHandling()
    {
        try
        {
            ThrowException();
        }
        catch (InvalidOperationException ex)
        {
            Console.WriteLine($"Exception handling test passed: {ex.Message}");
        }
    }

    static void ThrowException()
    {
        throw new InvalidOperationException("Intentional exception for testing.");
    }

    static void TestDependencyInjection()
    {
        var services = new ServiceCollection();
        ConfigureServices(services);
        var serviceProvider = services.BuildServiceProvider();
        var myService = serviceProvider.GetService<MyService>();
        myService.PerformAction();
    }

    static void ConfigureServices(IServiceCollection services)
    {
        services.AddSingleton<MyService>();
    }

    static void TestLocalization()
    {
        CultureInfo originalCulture = CultureInfo.CurrentCulture;
        try
        {
            CultureInfo.CurrentCulture = new CultureInfo("fr-FR");
            Console.WriteLine($"Current culture set to {CultureInfo.CurrentCulture.Name}");
            DateTime localDate = DateTime.Now;
            Console.WriteLine($"Local date format: {localDate.ToString("d")}");
        }
        finally
        {
            CultureInfo.CurrentCulture = originalCulture;
        }
    }

    static void TestMemoryManagement()
    {
        WeakReference weakReference = new WeakReference(new object());
        GC.Collect();
        Console.WriteLine($"Garbage collection test: Object is alive? {weakReference.IsAlive}");
    }

    static void TestEventHandling()
    {
        MyEventPublisher publisher = new MyEventPublisher();
        publisher.MyEvent += (sender, e) => Console.WriteLine($"Event triggered: {e.Message}");
        publisher.TriggerEvent();
    }

    static void TestDevTtyAccessibility()
    {
        try
        {
            using (FileStream fs = File.Open("/dev/tty", FileMode.Open, FileAccess.ReadWrite))
            {
                Console.WriteLine("/dev/tty is accessible.");
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"/dev/tty access error: {ex.Message}");
        }
    }

    static void TestUsbDevices()
    {
        Console.WriteLine("Available USB devices (serial ports):");
        string[] ports = SerialPort.GetPortNames();
        foreach (string port in ports)
        {
            Console.WriteLine($"- {port}");
        }
    }

    static void TestNetworkDevicesAccessibility()
    {
        Console.WriteLine("Available network interfaces:");
        foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
        {
            Console.WriteLine($"- {ni.Name}: {ni.NetworkInterfaceType}, Status: {ni.OperationalStatus}");
            IPInterfaceProperties properties = ni.GetIPProperties();
            foreach (IPAddressInformation uniCast in properties.UnicastAddresses)
            {
                Console.WriteLine($"  IP Address: {uniCast.Address}");
            }
        }
    }

    class MyService
    {
        public void PerformAction()
        {
            Console.WriteLine("Dependency injection test: Service is working.");
        }
    }

    class MyEventPublisher
{
    public event EventHandler<MyEventArgs>? MyEvent;  // Make event nullable to address CS8618

    public void TriggerEvent()
    {
        MyEvent?.Invoke(this, new MyEventArgs("Test event"));
    }
}

class MyEventArgs : EventArgs
{
    public string Message { get; }

    public MyEventArgs(string message)
    {
        Message = message;
    }
}
}
